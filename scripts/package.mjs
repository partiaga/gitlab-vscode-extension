import path from 'path';
import { root } from './utils/run_utils.mjs';
import { buildDesktop, buildPackage, copyNodeModules } from './utils/desktop_jobs.mjs';

async function build() {
  await buildDesktop();
  await copyNodeModules();
  await buildPackage({ cwd: path.resolve(root, 'dist-desktop') });
}

build();
