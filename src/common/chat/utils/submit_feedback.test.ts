import * as vscode from 'vscode';
import { GitLabEnvironment } from '../get_platform_manager_for_chat';
import { SubmitFeedbackParams, submitFeedback } from './submit_feedback';

jest.mock('../../snowplow/snowplow', () => ({
  Snowplow: {
    getInstance: jest.fn().mockReturnValue({
      trackStructEvent: jest.fn(),
    }),
  },
}));

const { trackStructEvent } = jest.requireMock('../../snowplow/snowplow').Snowplow.getInstance();

const extension = vscode.extensions.getExtension('Gitlab.gitlab-workflow');

const IDE_EXTENSION_CONTEXT = {
  schema: 'iglu:com.gitlab/ide_extension_version/jsonschema/1-0-0',
  data: {
    ide_name: 'Visual Studio Code',
    ide_vendor: 'Microsoft Corporation',
    ide_version: vscode.version,
    extension_name: 'GitLab Workflow',
    extension_version: extension?.packageJSON.version,
  },
};

describe('submitFeedback', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  describe('with feedback', () => {
    it('sends snowplow event', async () => {
      const expected: SubmitFeedbackParams = {
        extendedTextFeedback: 'Freetext feedback',
        feedbackChoices: ['helpful', 'fast'],
        gitlabEnvironment: GitLabEnvironment.GITLAB_COM,
      };

      await submitFeedback(expected);

      const standardContext = {
        schema: 'iglu:com.gitlab/gitlab_standard/jsonschema/1-0-9',
        data: {
          source: 'gitlab-vscode',
          extra: {
            extended_feedback: expected.extendedTextFeedback,
          },
          environment: expected.gitlabEnvironment,
        },
      };

      expect(trackStructEvent).toHaveBeenCalledWith(
        {
          category: 'ask_gitlab_chat',
          action: 'click_button',
          label: 'response_feedback',
          property: 'helpful,fast',
        },
        [standardContext, IDE_EXTENSION_CONTEXT],
      );
    });

    it('sends snowplow event when choices are null', async () => {
      const expected: SubmitFeedbackParams = {
        extendedTextFeedback: 'Freetext feedback',
        feedbackChoices: null,
        gitlabEnvironment: GitLabEnvironment.GITLAB_COM,
      };

      await submitFeedback(expected);

      const standardContext = {
        schema: 'iglu:com.gitlab/gitlab_standard/jsonschema/1-0-9',
        data: {
          source: 'gitlab-vscode',
          extra: {
            extended_feedback: expected.extendedTextFeedback,
          },
          environment: expected.gitlabEnvironment,
        },
      };

      expect(trackStructEvent).toHaveBeenCalledWith(
        {
          category: 'ask_gitlab_chat',
          action: 'click_button',
          label: 'response_feedback',
        },
        [standardContext, IDE_EXTENSION_CONTEXT],
      );
    });

    it('sends snowplow event when free text feedback is null', async () => {
      const expected: SubmitFeedbackParams = {
        extendedTextFeedback: null,
        feedbackChoices: ['helpful', 'fast'],
        gitlabEnvironment: GitLabEnvironment.GITLAB_COM,
      };

      await submitFeedback(expected);

      const standardContext = {
        schema: 'iglu:com.gitlab/gitlab_standard/jsonschema/1-0-9',
        data: {
          source: 'gitlab-vscode',
          extra: {
            extended_feedback: expected.extendedTextFeedback,
          },
          environment: expected.gitlabEnvironment,
        },
      };

      expect(trackStructEvent).toHaveBeenCalledWith(
        {
          category: 'ask_gitlab_chat',
          action: 'click_button',
          label: 'response_feedback',
          property: 'helpful,fast',
        },
        [standardContext, IDE_EXTENSION_CONTEXT],
      );
    });
  });

  describe('with empty feedback', () => {
    const emptySubmitFeedbackParams: SubmitFeedbackParams = {
      extendedTextFeedback: '',
      feedbackChoices: [],
      gitlabEnvironment: GitLabEnvironment.GITLAB_COM,
    };

    it('does not send a snowplow event', async () => {
      await submitFeedback(emptySubmitFeedbackParams);

      expect(trackStructEvent).not.toHaveBeenCalled();
    });

    it('does not send a snowplow event when free text feedback and choices are null', async () => {
      await submitFeedback(emptySubmitFeedbackParams);

      expect(trackStructEvent).not.toHaveBeenCalled();
    });
  });
});
