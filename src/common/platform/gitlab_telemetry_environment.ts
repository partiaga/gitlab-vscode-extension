import vscode from 'vscode';

export interface GitLabTelemetryEnvironment {
  isTelemetryEnabled(): boolean;
  onDidChangeTelemetryEnabled: vscode.Event<boolean>;
  dispose?: () => void;
}
