import vscode from 'vscode';
import {
  API_ERROR_NOTIFICATION,
  API_RECOVERY_NOTIFICATION,
  IClientConfig,
  TELEMETRY_NOTIFICATION,
  TRACKING_EVENTS,
  TokenCheckResponse,
} from '@gitlab-org/gitlab-lsp';
import { BaseLanguageClient, DidChangeConfigurationNotification } from 'vscode-languageclient';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import {
  getExtensionConfiguration,
  getAiAssistedCodeSuggestionsConfiguration,
  getHttpAgentConfiguration,
} from '../utils/extension_configuration';
import { log } from '../log';
import { FeatureFlag, isEnabled } from '../feature_flags';
import { GitLabTelemetryEnvironment } from '../platform/gitlab_telemetry_environment';

export class LanguageClientWrapper {
  #client: BaseLanguageClient;

  #suggestionsManager: GitLabPlatformManagerForCodeSuggestions;

  #stateManager: CodeSuggestionsStateManager;

  #telemetryEnvironment: GitLabTelemetryEnvironment;

  #subscriptions: vscode.Disposable[] = [];

  constructor(
    client: BaseLanguageClient,
    suggestionsManager: GitLabPlatformManagerForCodeSuggestions,
    stateManager: CodeSuggestionsStateManager,
    telemetryEnvironment: GitLabTelemetryEnvironment,
  ) {
    this.#client = client;
    this.#suggestionsManager = suggestionsManager;
    this.#stateManager = stateManager;
    this.#telemetryEnvironment = telemetryEnvironment;
  }

  async initAndStart() {
    this.#client.registerProposedFeatures();
    this.#subscriptions.push();
    // TODO: export `NotificationType`s from LSP
    // https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/blob/v1.0.1/src/common/connection.ts#L61
    this.#client.onNotification(
      '$/gitlab/token/check',
      ({ message, reason }: TokenCheckResponse) => {
        log.warn(
          `Token validation failed in Language Server: (${message} - ${reason}). This can happen with OAuth token refresh. If the rest of the extension works, this won't be a problem.`,
        );
      },
    );

    this.#client.onNotification(API_ERROR_NOTIFICATION, () => this.#stateManager.setError(true));
    this.#client.onNotification(API_RECOVERY_NOTIFICATION, () =>
      this.#stateManager.setError(false),
    );
    await this.#client.start();
    this.#subscriptions.push({ dispose: () => this.#client.stop() });
    await this.syncConfig();
  }

  syncConfig = async () => {
    const platform = await this.#suggestionsManager.getGitLabPlatform();
    if (!platform) {
      log.warn('There is no GitLab account available with access to suggestions');
      return;
    }
    const extensionConfiguration = getExtensionConfiguration();
    const httpAgentConfiguration = getHttpAgentConfiguration();
    const settings: IClientConfig = {
      baseUrl: platform.account.instanceUrl,
      token: platform.account.token,
      telemetry: {
        enabled: this.#telemetryEnvironment.isTelemetryEnabled(),
        actions: [{ action: TRACKING_EVENTS.ACCEPTED }],
      },
      featureFlags: {
        [FeatureFlag.CodeSuggestionsClientDirectToGateway]: isEnabled(
          FeatureFlag.CodeSuggestionsClientDirectToGateway,
        ),
        [FeatureFlag.StreamCodeGenerations]: isEnabled(FeatureFlag.StreamCodeGenerations),
      },
      suggestionsCache: getAiAssistedCodeSuggestionsConfiguration().suggestionsCache,
      codeCompletion: {
        additionalLanguages: getAiAssistedCodeSuggestionsConfiguration().additionalLanguages,
      },
      logLevel: extensionConfiguration.debug ? 'debug' : 'info',
      projectPath: platform.project?.namespaceWithPath ?? '',
      ignoreCertificateErrors: extensionConfiguration.ignoreCertificateErrors,
      httpAgentOptions: {
        ...httpAgentConfiguration,
      },
    };

    log.info(`Configuring Language Server - baseUrl: ${platform.account.instanceUrl}`);
    await this.#client.sendNotification(DidChangeConfigurationNotification.type, {
      settings,
    });
  };

  sendSuggestionAcceptedEvent = async (trackingId: string, optionId?: number) =>
    this.#client.sendNotification(TELEMETRY_NOTIFICATION, {
      category: 'code_suggestions',
      action: TRACKING_EVENTS.ACCEPTED,
      context: { trackingId, optionId },
    });

  dispose = () => {
    this.#subscriptions.forEach(s => s.dispose());
  };
}
